# ---
# jupyter:
#   jupytext:
#     cell_metadata_filter: -all
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.15.2
# ---

# %% [markdown]
# # Mandat 2: Statistiques

# %%
import math
import matplotlib.pyplot as plt
plt.style.use('grayscale')
import statistics
from scipy import stats as st

# %%
# Fonctions du mandat 2
def printDict(dictionary):
  longest_key = max(len(k) for k in dictionary)
  for k, v in dictionary.items():
    print("{:>{width}} :".format(k, width=longest_key), v)


def mode(array):
  most = max(list(map(array.count, array)))
  return list(set(filter(lambda x: array.count(x) == most, array)))


def conditionnalCount(array, limInf, limSup):
  count = []
  for val in array:
    if val >= limInf and val < limSup:
        count.append(val)
  return len(count)


def calculBorneFromZ(moyenne, ecartype, Z, nbEch):
  return moyenne + Z * (ecartype/math.sqrt(nbEch))


def calculBorneFromZSansRacineN(moyenne, ecartype, Z):
  return moyenne + Z * (ecartype)


def calculZFromBorne(moyenne, ecartype, cibleX, nbEch):
  return (cibleX - moyenne) / (ecartype/math.sqrt(nbEch))


def borneConfianceMoyenne(moyenne, ecartype, cibleX, nbEch):
  a = 1 - cibleX
  borneXInf = a / 2
  borneXSup = 1 - borneXInf
  Z = st.norm.ppf(borneXSup)

  borneInf = calculBorneFromZ(moyenne, ecartype, -Z, nbEch)
  borneSup = calculBorneFromZ(moyenne, ecartype, Z, nbEch)

  bornes = [borneInf, borneSup]
  print("Bornes =", bornes)
  return bornes


def findZ0TestStatistiqueMoyenneNormale(esp, mu0, sigma, nbEch):
  z0 = (esp-mu0)/(sigma/math.sqrt(nbEch))
  print("z0 =", z0)
  return z0


def isRejectedHypotheseZ0ltZalpha(z0, alpha):
  zAlpha = st.norm.ppf(alpha)
  print("zAlpha =", zAlpha)

  if (z0 < zAlpha):
    print("z0 < zAlpha: On rejete l'hypothèse H0.")
    return True
  else:
    print("z0 >= zAlpha: On ne rejete pas l'hypothèse H0.")
    return False

def findChi20TestStatistiqueVarianceNormale(var, var0, nbEch):
  chi20 = ((nbEch - 1)*var)/var0
  print("chi2_0 =", chi20)
  return chi20

def isRejectedHypotheseChi20neqChi2alpha(chi20, alpha, nbEch):
  chi2AlphaInf = st.chi2.ppf(alpha, nbEch - 1)
  chi2AlphaSup = st.chi2.ppf(1 - alpha, nbEch - 1)
  print("chi2AlphaInf =", chi2AlphaInf)
  print("chi2AlphaSup =", chi2AlphaSup)

  if (chi20 > chi2AlphaSup):
    print("chi20 > chi2AlphaSup: On rejete l'hypothèse H0.")
    return True
  elif (chi20 < chi2AlphaInf):
    print("chi20 < chi2AlphaInf: On rejete l'hypothèse H0.")
    return True
  else:
    print("chi2AlphaInf <= chi20 <= chi2AlphaSup: On ne rejete pas l'hypothèse H0.")
    return False

# %% [markdown]
# ## Tableau des statistiques descriptives

# %%
with open("TempsDeJeu.txt") as f:
  tempsJeu = []
  for line in f:
    tempsJeu.append(float(line.strip()))

statTempsJeu = {}
statTempsJeu["Moyenne"] = statistics.mean(tempsJeu)
statTempsJeu["Médiane"] = statistics.median(tempsJeu)
statTempsJeu["Mode"] = mode(tempsJeu)
statTempsJeu["Écart type"] = statistics.stdev(tempsJeu)
statTempsJeu["Variance"] = statistics.variance(tempsJeu)
statTempsJeu["Minimum"] = min(tempsJeu)
statTempsJeu["Maximum"] = max(tempsJeu)
statTempsJeu["Étendue"] = max(tempsJeu) - min(tempsJeu)
statTempsJeu["Nombre d'échantillion"] = len(tempsJeu)

printDict(statTempsJeu)

# %% [markdown]
# ## Histogramme

# %%
# Sturges suggère 8 classes, la méthode de la racine carré proposé par le manuel suggère 10 classes.
nbBins = 9

# Avec une étendue de 234, on obtient une amplitude entre 29,25 pour 8 classes et 23,4 pour 10 classes.
# Avec une amplitude de 25, on obtient 11 classes, ce qui est légèrement trop élevé.
# Avec une amplitude de 50, on obtient 6 classes, ce qui est trop peu.
# Une amplitude de 30 semble raisonnable.
binWidth = 30

# Le premier multiple de 30 inférieur à 148 est 120.
firstBin = 120
bins = []
for i in range(nbBins + 1):
  bins.append(i*binWidth + firstBin)

print("Classe", "Limites ", "Centres", "Freq.", "F. rel.", "F. abs.", sep='\t')
for i in range(len(bins) - 1):
  limInf = bins[i]
  limSup = bins[i+1]
  limite = [limInf, limSup]
  centre = (limSup - limInf) / 2 + limInf
  freq = conditionnalCount(tempsJeu, limInf, limSup)
  freqRel = freq / len(tempsJeu)
  freqCum = conditionnalCount(tempsJeu, min(tempsJeu), limSup)
  freqAbs = freqCum / len(tempsJeu)
  print(i, limite, centre, freq, freqRel, freqAbs, sep='\t')

fig = plt.hist(tempsJeu, bins)

# %% [markdown]
# ## Intervalle de confiance

# %%
cible = 0.95
borneConfianceMoyenne(
  moyenne = statTempsJeu["Moyenne"],
  ecartype = statTempsJeu["Écart type"],
  cibleX = cible,
  nbEch = statTempsJeu["Nombre d'échantillion"]
)

# %% [markdown]
# ## Test d'hypothèse sur la moyenne
#
# 1. On souhaite effectuer un test sur la moyenne de la population avec la moyenne de l'échantillon.
# 2. H0: $\mu>=300$
# 3. H1: $\mu<300$
# 4. Puisque la variance est inconnue et que notre échantillon contient plus de 30 valeurs, on utilise: $Z_0 = \dfrac{\overline{X}-\mu_0}{{S}/{\sqrt{n}}}$
# 5. On rejete H0 si $Z_0<-Z_\alpha$

# %%
# 6. Calculs
cible = 0.95
z0 = findZ0TestStatistiqueMoyenneNormale(
  esp = statTempsJeu["Moyenne"],
  mu0 = 300,
  sigma = statTempsJeu["Écart type"],
  nbEch = statTempsJeu["Nombre d'échantillion"]
)

# 7.Conclusion
isRejectedHypotheseZ0ltZalpha(z0, 1 - cible)

# %% [markdown]
# ## Erreur de première espèce

# %%
cible = 1 - 0.95
zRejet = st.norm.ppf(cible)
borneDeRejet = calculBorneFromZ(
  moyenne = 300,
  ecartype = statTempsJeu["Écart type"],
  Z = zRejet,
  nbEch = statTempsJeu["Nombre d'échantillion"]
)
print("Borne de rejet =", borneDeRejet)

probabilite = st.norm.cdf(zRejet)
print("Probabilite =", probabilite,"=",probabilite*100, "%")

# %% [markdown]
# ## Erreur de deuxième espèce

# %%
cible = 1 - 0.95
zRejet = st.norm.ppf(cible)
borneDeRejet = calculBorneFromZ(
  moyenne = 300,
  ecartype = statTempsJeu["Écart type"],
  Z = zRejet,
  nbEch = statTempsJeu["Nombre d'échantillion"]
)
print("Borne de rejet =", borneDeRejet)

zEch = calculZFromBorne(
  moyenne = statTempsJeu["Moyenne"],
  ecartype = statTempsJeu["Écart type"],
  cibleX = borneDeRejet,
  nbEch = statTempsJeu["Nombre d'échantillion"]
)

probabilite = 1 - st.norm.cdf(zEch)
print("Probabilite =", probabilite,"=",probabilite*100, "%")

# %% [markdown]
# ## Test d'hypothèse bilatéral sur la variance
#
# 1. On souhaite effectuer un test sur la variance des temps de jeux avec un seuil de signification de 5%.
# 2. H0: $\sigma^2 = 50$
# 3. H1: $\sigma^2 \neq 50$
# 4. La statistique de test est: $\chi^2_0 = \dfrac{(n - 1) S^2}{\sigma^2_0}$
# 5. Avec $\alpha=0,05$, H0 est rejetée si $\chi^2_0 > \chi^2_{\alpha/2;n-1}$ ou si $\chi^2_0 < \chi^2_{1-\alpha/2;n-1}$

# %%
# 6. Calculs
seuil = 0.05
chi20 = findChi20TestStatistiqueVarianceNormale(
  var = statTempsJeu["Variance"],
  var0 = 50,
  nbEch = statTempsJeu["Nombre d'échantillion"]
)

# 7.Conclusion
isRejectedHypotheseChi20neqChi2alpha(
  chi20 = chi20,
  alpha = seuil,
  nbEch = statTempsJeu["Nombre d'échantillion"]
)

# %% [markdown]
# ## Test d’ajustement
#
# Visuellement, l'histogramme semble présenter une distribution normale. Faisons un test d'ajustement pour le déterminer de façon quantitative.
#
# 1. La variable d'intérêt est la forme de la distribution des temps de jeu.
# 2. H0: la forme de la distribution des temps de jeux est une distribution normale.
# 3. H1: la forme de la distribution des temps de jeux n'est pas une distribution normale.
# 4. La statistique de test est: $\chi^2_0  = \sum^k_{i=0}\frac{(O_i - E_i)}{E_i}$
# 5. On rejete H0 si $\chi^2_0 est > à 11,07$

# %%
# 6. Calculs
# Z choisis pour 8 segmenents d'égale probabilités, selon l'exemple 9.13 du manuel
zeq8 = [
  st.norm.ppf(0),
  -1.15,
  -0.675,
  -0.32,
  0.0,
  0.32,
  0.675,
  1.15,
  st.norm.ppf(1)
]

moyenne = statTempsJeu["Moyenne"]
ecartype = statTempsJeu["Écart type"]
nbEch = statTempsJeu["Nombre d'échantillion"]

partialSum = 0
for i in range(len(zeq8) - 1):
  zInf = zeq8[i]
  zSup = zeq8[i + 1]

  borneInf = calculBorneFromZSansRacineN(moyenne, ecartype, zInf)
  borneSup = calculBorneFromZSansRacineN(moyenne, ecartype, zSup)

  freqObs = conditionnalCount(tempsJeu, borneInf, borneSup)

  probInf = st.norm.cdf(zInf)
  probSup = st.norm.cdf(zSup)
  prob = probSup - probInf
  freqEsp = prob * nbEch

  partialSum += ((freqObs - freqEsp)**2)/freqEsp

  print(freqEsp)
  print(partialSum)

chi20 = partialSum

print("chi20 = ", chi20)
k = 8-2-1
Chi = 11.07

# 7.Conclusion
print("On ne rejette pas H0 puisque chi20 est < à 11,07")
