# ---
# jupyter:
#   jupytext:
#     cell_metadata_filter: -all
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.15.2
# ---

# %% [markdown]
# # Mandat 3: Simulations Monte-Carlo

# %%
import numpy as np

# %%
# Fonctions du mandat 3

# %% [markdown]
# ## Générerez au moins 10 000 réalisations de P

# %%

N = 10000

λ1 = 15
λ2 = 50
λ3 = 95

# Variable aléatoire exponantielle P
Fp = np.random.rand(N, 1)
P1 = (-(np.log(1 - Fp))) / λ1
P2 = (-(np.log(1 - Fp))) / λ2
P3 = (-(np.log(1 - Fp))) / λ3

# %% [markdown]
# ## Tirez une valeur de Q pour chaque branchement

# %%
# Variable aléatoire normale Q
mu = 280.58
sigma = 50.38
Z = np.random.randn(N)
Q1 = (sigma * Z) + mu

#Instants de branchements des joueurs
tempsArrive1 = np.cumsum(P1)
tempsArrive2 = np.cumsum(P2)
tempsArrive3 = np.cumsum(P3)

#Valeur de Q pour chaque instant de branchement
branchementT1 = tempsArrive1 + Q1
branchementT2 = tempsArrive2 + Q1
branchementT3 = tempsArrive3 + Q1

# %% [markdown]
# ## Observation des deux histogrammes

# %%
#Déterminer nombre de personne selon un temps
valeur = 0
tab1 = []
tab2 = []
tab3 = []

for i in range(0,int(max(branchementT1)+3),1):
    for k in range(0,10000):
        if(branchementT1[k] >= i and tempsArrive1[k] <=i):
            valeur = valeur + 1
    tab1.append(valeur)
    valeur = 0

valeur = 0
for i_2 in range(0, int(max(branchementT2)+3), 1):
    for k_2 in range(0, 10000):
        if (branchementT2[k_2] >= i_2 and tempsArrive2[k_2] <= i_2):
            valeur = valeur + 1
    tab2.append(valeur)
    valeur = 0

valeur = 0
for i_3 in range(0, int(max(branchementT3)+3), 1):
    for k_3 in range(0, 10000):
        if (branchementT3[k_3] >= i_3 and tempsArrive3[k_3] <= i_3):
            valeur = valeur + 1
    tab3.append(valeur)
    valeur = 0


plt.title(f'Distribution exponentielle (λ={λ1})')
plt.xlabel('Temps entre arrivées (P1)')
plt.hist(P1, bins=30, density=True, color='r', ec='g')
plt.show()

plt.title(f'Distribution exponentielle (λ={λ2})')
plt.xlabel('Temps entre arrivées (P2)')
plt.hist(P2, bins=30, density=True, color='r', ec='g')
plt.show()

plt.title(f'Distribution exponentielle (λ={λ3})')
plt.xlabel('Temps entre arrivées (P3)')
plt.hist(P3, bins=30, density=True, color='r', ec='g')
plt.show()

plt.title('Distribution normale (Q)')
plt.xlabel('Temps de jeu (Q)')
plt.hist(Q1, bins=30, density=True, color='g', ec='r')
plt.show()

# %% [markdown]
# ## Déterminez un nombre moyen de joueurs branchés à un temps donné, en fonction de lambda (λ) (au moins 3 valeurs de lambda (λ))

# %%
#Nombre moyen de joueurs branchés à un temps donné
Moyenne_joueur1 = np.mean(tab1)
Moyenne_joueur2 = np.mean(tab2)
Moyenne_joueur3 = np.mean(tab3)
print("Pour un interval de temps de 0 à ",int(max(branchementT1)+5),"pour une meilleur précision")
print("Nombre moyen de joueurs branchés à un temps donné (λ=",λ1,"): ",Moyenne_joueur1," joueurs")
print("Pour un interval de temps de 0 à ", int(max(branchementT2) + 5),"pour une meilleur précision")
print("Nombre moyen de joueurs branchés à un temps donné (λ=", λ2, "): ", Moyenne_joueur2, " joueurs")
print("Pour un interval de temps de 0 à ", int(max(branchementT3) + 5),"pour une meilleur précision")
print("Nombre moyen de joueurs branchés à un temps donné (λ=", λ3, "): ", Moyenne_joueur3, " joueurs")
