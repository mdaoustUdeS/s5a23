# ---
# jupyter:
#   jupytext:
#     cell_metadata_filter: -all
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.15.2
# ---

# %% [markdown]
# # Mandat 1: Probabilités

# %%
import math
from scipy import stats as st

# %%
# Fonctions du mandat 1
def probabiliteSimple(nbEventTotal, nbEventGagnant):
  print("Probabilite =", nbEventGagnant,"/",nbEventTotal)

  probabilite = nbEventGagnant / nbEventTotal
  print("Probabilite =", probabilite,"=",probabilite*100, "%")
  return probabilite


def permutationSousEnsemble(nbElement, nbElementSelection):
  n = nbElement
  r = nbElementSelection
  print("nbPermutation =", n,"!","/",n-r,"!")

  nbPermutation = math.factorial(n)/math.factorial(n-r)
  print("nbPermutation =", nbPermutation)
  return nbPermutation


def combinaison(nbElement, nbElementCible):
  n = nbElement
  r = nbElementCible
  print("nbCombinaison =", n,"!","/","(",r,"!","*",n-r,"!",")")

  nbCombinaison = math.factorial(n)/(math.factorial(r)*math.factorial(n-r))
  print("nbCombinaison =", nbCombinaison)
  return nbCombinaison


def probabiliteBinomiale(probChaqueEssaieGagnant, nbEssaieTotal, nbEssaieCible):
  p = probChaqueEssaieGagnant
  n = nbEssaieTotal
  x = nbEssaieCible
  print("Probabilite = Combinaison(",n,",",x,") * (", p,"^",x,") * (",1-p,"^",n-x,")")

  probabilite = combinaison(n,x) * (p**x) * ((1-p)**(n-x))
  print("Probabilite =", probabilite,"=",probabilite*100, "%")
  return probabilite


def moyenneBinomiale(probChaqueEssaieGagnant, nbEssaieTotal):
  p = probChaqueEssaieGagnant
  n = nbEssaieTotal
  print("moyenne =",n,"*",p)

  moyenne = n*p
  print("moyenne =", moyenne)
  return moyenne


def varianceBinomiale(probChaqueEssaieGagnant, nbEssaieTotal):
  p = probChaqueEssaieGagnant
  n = nbEssaieTotal
  print("variance =",n,"*",p,"*",1-p)

  variance = n*p*(1-p)
  print("variance =", variance)
  return variance


def probabiliteNormale(moyenne, ecartype, cible):
  mu = moyenne
  sigma = ecartype
  X = cible
  Z = (X-mu)/sigma

  probabilite = st.norm.cdf(Z)
  print("Probabilite =", probabilite,"=",probabilite*100, "%")
  return probabilite

# %% [markdown]
# ## Probabilités pour acquérir un pouvoir: approche #1

# %% [markdown]
# ### Cas #1: 3 roues ayant chacune 8 pictogrammes

# %%
probCas1 = probabiliteSimple(8**3,8)

# %% [markdown]
# ### Cas #2: 4 roues ayant chacune 5 pictogrammes

# %%
probCas2 =  probabiliteSimple(5**4,5)

# %% [markdown]
# ## Probabilités pour acquérir un pouvoir: approche #2

# %% [markdown]
# ### Cas #1: 3 roues ayant chacune 8 pictogrammes

# %%
probabiliteSimple(8**3,permutationSousEnsemble(8,3))

# %% [markdown]
# ### Cas #2: 4 roues ayant chacune 5 pictogrammes

# %%
probabiliteSimple(5**4,permutationSousEnsemble(5,4))

# %% [markdown]
# ## Probabilités pour acquérir un pouvoir: approche #3

# %% [markdown]
# ### Cas #1: 3 roues ayant chacune 8 pictogrammes

# %%
# Calcul pour aucune fois
probCas1_3_0 = probabiliteBinomiale(probCas1,5,0)

# %%
# Calcul pour une seule fois
probCas1_3_1 = probabiliteBinomiale(probCas1,5,1)

# %%
# Calcul pour deux fois ou plus
probCas1_3 = (1 - (probCas1_3_0 + probCas1_3_1))
print("Probabilite =", probCas1_3,"=",probCas1_3*100, "%")

# %%
# Calcul de la moyenne
moyenneBinomiale(probCas1,5)

# %%
# Calcul de la variance
varianceBinomiale(probCas1,5)

# %% [markdown]
# ### Cas #2: 4 roues ayant chacune 5 pictogrammes

# %%
# Calcul pour aucune fois
probCas2_3_0 = probabiliteBinomiale(probCas2,5,0)

# %%
# Calcul pour une seule fois
probCas2_3_1 = probabiliteBinomiale(probCas2,5,1)

# %%
# Calcul pour deux fois ou plus
probCas2_3 = (1 - (probCas2_3_0 + probCas2_3_1))
print("Probabilite =", probCas2_3,"=",probCas2_3*100, "%")

# %%
# Calcul de la moyenne
moyenneBinomiale(probCas2,5)

# %%
# Calcul de la variance
varianceBinomiale(probCas2,5)

# %% [markdown]
# ## Probabilités conjointes

# %%
rayon = 1
moyenneX = 0
moyenneY = 0
ecartX = 0.1
ecartYnear = 0.05
ecartYfar = 0.4
rayonTarget = 0.1

# %% [markdown]
# ### Indépendance de X et Y
#
# Les variables aléatoires x et y sont indépendantes puisque les deux variables ont une distribution normale bivarié et leur corrélation est nulle. On peut affirmer que la corrélation entre les deux variables aléatoires est nulle puisque le grand axe de l'élipse formée par des points de la fonction de densité normale est colinéaire avec l'abscisse x ou l'ordonnée y.

# %% [markdown]
# ### Covariance entre X et Y

# %%
# Cas #1 et #2 : z < 1m et z > 10 m
print("cov = 0 (voir eq. 5.13)")

# %% [markdown]
# ### Corrélation entre X et Y

# %%
# Cas #1 et #2 : z < 1m et z > 10 m
print("corelation = 0 (voir eq. 5.13)")

# %% [markdown]
# ### Fonction de densité de probabilité conditionnelle f(x,y|z)
# $$
# \begin{split}
#   f(x,y|z) &= \frac{1}{2\pi\sigma_x\sigma_y}\times e^{-\frac{x^2}{2\sigma_x^2}-\frac{y^2}{2\sigma_y^2}}\\
#   \\
#   f(x,y|z,z<1) &= \frac{1000}{\pi}\times e^{-50x^2-200y^2}\\
#   \\
#   f(x,y|z,z>10) &= \frac{12,5}{\pi}\times e^{-50x^2-3,125y^2}
# \end{split}
# $$

# %% [markdown]
# ### Fonctions de densité de probabilité marginale f(x|z)
# $$
# \begin{split}
#   f(x|z) &= \frac{1}{\sigma_x\sqrt{2\pi}}\times e^{\frac{-(x-\mu_x)^2}{2\sigma_x^2}}\\
#   \\
#   f(x|z) &= \frac{1}{0,1\sqrt{2\pi}}\times e^{-50x^2}
# \end{split}
# $$

# %% [markdown]
# ### Fonctions de densité de probabilité marginale f(y|z)
# $$
# \begin{split}
#   f(y|z) &= \frac{1}{\sigma_y\sqrt{2\pi}}\times e^{\frac{-(x-\mu_y)^2}{2\sigma_y^2}}\\
#   \\
#   f(y|z,z<1) &= \frac{1}{0,05\sqrt{2\pi}}\times e^{-200y^2}\\
#   \\
#   f(y|z,z>10) &= \frac{1}{0,4\sqrt{2\pi}}\times e^{-3,125y^2}
# \end{split}
# $$

# %% [markdown]
# ### Probabilité que le personnage ouvre la porte < 1m

# %%
# Calcul pour x
probX1 = probabiliteNormale(moyenneX, ecartX, -rayonTarget)
probX2 = probabiliteNormale(moyenneX, ecartX, rayonTarget)
probX = probX2 - probX1
print("Probabilite x =", probX,"=",probX*100, "%")

# %%
# Calcul pour y (z < 1)
probYnear1 = probabiliteNormale(moyenneY, ecartYnear, -rayonTarget)
probYnear2 = probabiliteNormale(moyenneY, ecartYnear, rayonTarget)
probYnear = probYnear2 - probYnear1
print("Probabilite y (z < 1) =", probYnear,"=",probYnear*100, "%")

# %%
# Calcul pour y (z > 10)
probYfar1 = probabiliteNormale(moyenneY, ecartYfar, -rayonTarget)
probYfar2 = probabiliteNormale(moyenneY, ecartYfar, rayonTarget)
probYfar = probYfar2 - probYfar1
print("Probabilite y (z > 10) =", probYfar,"=",probYfar*100, "%")

# %%
# Calcul pour probabilité proche (z < 1)
probNear = probX * probYnear
print("Probabilite proche =", probNear,"=",probNear*100, "%")

# %% [markdown]
# ### Probabilité que le personnage ouvre la porte > 10m

# %%
# Calcul pour probabilité loin (z > 10)
probFar = probX * probYfar
print("Probabilite loin =", probFar,"=",probFar*100, "%")
